{-# LANGUAGE KindSignatures #-}
module TypeSynMono where

import Data.Kind

type F x = x :: Type -> Type

-- No polykinds, should default to matchable
type Test = F Maybe
