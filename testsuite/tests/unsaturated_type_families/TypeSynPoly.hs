{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE PolyKinds #-}
module TypeSynPoly where

import Data.Kind

type F x = x :: Type -> Type

type Id a = a

type Test = F Maybe
type Test2 = F Id
