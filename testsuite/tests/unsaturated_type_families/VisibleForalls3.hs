{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE StandaloneKindSignatures #-}
module VisibleForalls3 where

import Data.Kind
import GHC.Types

type family F1 (k :: Type) :: k where {}

-- This should fail, but not panic
type F2 :: forall m -> forall (k :: Type) -> @m k
type family F2 m where
  F2 m = F1
