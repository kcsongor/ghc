{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module CUSK2 where

import Data.Kind

type family Id a where
  Id a = a

type family Cusk :: (Type -> @m Type) where
  Cusk = Maybe
  Cusk = Id
