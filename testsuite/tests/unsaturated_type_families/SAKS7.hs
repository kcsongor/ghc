{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# OPTIONS_GHC -fprint-explicit-matchabilities #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module SAKS7 where

import Data.Kind
import GHC.Exts

-- polymorphic
type Yay :: (Type -> Type) -> Type -> Type
type Yay f = f

type Id a = a

type Yay1 = Yay Id
type Yay2 = Yay Maybe

