{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeFamilies #-}
module SnyonymInPattern2 where

import Data.Kind

type Id (x :: Type) = x
type Const (x :: a) (y :: b) = x

type family C (a :: k) :: k where
  C (a :: Const k Id) = a
