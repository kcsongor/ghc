{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# OPTIONS_GHC -fprint-explicit-matchabilities #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module SAKS3 where

import Data.Kind
import GHC.Exts

type family Id a where
  Id a = a

-- errors: we don't infer matchability-indexed kinds
type Foo :: Type -> Type
type family Foo :: Type -> Type where
  Foo = Maybe
  Foo = Id
