{-# LANGUAGE CPP #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module Bug23b where

import Data.Kind
import GHC.Exts

type Hm :: (a ~> Bool) -> a -> Bool
type family Hm f x where
  Hm f x = HmCase (f @@ x)

type HmCase :: Bool -> Bool
type family HmCase b where
  HmCase True  = False
  HmCase False = True

sHm :: forall a (f :: a ~> Bool) (x :: a).
       Sing f -> Sing x -> Sing (Hm f x)
sHm sF sX = case sF @@ sX of
  STrue  -> SFalse
  SFalse -> STrue

-----

type Sing :: k -> Type
type family Sing :: k -> Type

type SBool :: Bool -> Type
data SBool z where
  SFalse :: SBool False
  STrue  :: SBool True
type instance Sing = SBool

type SLambda :: forall k1 k2. (k1 ~> k2) -> Type
newtype SLambda (f :: k1 ~> k2) =
  SLambda { (@@) :: forall t. Sing t -> Sing (f @@ t) }
type instance Sing = SLambda

type (~>) :: Type -> Type -> Type
type a ~> b = a -> @U b

type (@@) :: (k1 ~> k2) -> k1 -> k2
type family f @@ x where
  f @@ x = f x
