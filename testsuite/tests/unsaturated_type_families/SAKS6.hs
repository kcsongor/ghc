{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# OPTIONS_GHC -fprint-explicit-matchabilities #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module SAKS6 where

import Data.Kind
import GHC.Exts

type Qux :: (Type -> Type) -> Constraint
type family Qux where
  Qux = Monad
