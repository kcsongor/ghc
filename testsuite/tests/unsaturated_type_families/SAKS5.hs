{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# OPTIONS_GHC -fprint-explicit-matchabilities #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module SAKS5 where

import Data.Kind
import GHC.Exts

type family Id a where
  Id a = a

-- inferred kind unmatchable
type family Foo :: Type -> Type where
  Foo = Id
