{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
module Bug23 where

import Data.Kind
import Data.Proxy
import Data.Type.Equality
import GHC.Exts

type family F (x :: Type) :: Type where
  F Int = Bool
  F _ = Int

test :: forall (f :: Type -> @U Type) x.
        Proxy f -> Proxy x ->
        f x :~: Int -> F (f x) :~: Bool
test _ _ Refl = Refl

type family Id a where
  Id a = a

o :: ((f :: Type -> @U Type -> @U Type -> @U Type) a b ~ Id) => f a b Int -> f a b Bool
o = odd
