{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# OPTIONS_GHC -fprint-explicit-matchabilities #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module SAKS9 where

import Data.Kind
import GHC.Exts
import GHC.TypeLits

type Id a = a

type Poly :: Nat -> (Type -> Type) -> Type
type family Poly n f where
  Poly 0 m = m Int
  Poly 1 f = f (Poly 0 Maybe)
  Poly n f = f (Poly (n - 1) Id)
