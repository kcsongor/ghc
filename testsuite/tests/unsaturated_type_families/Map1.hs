{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# OPTIONS_GHC -fprint-explicit-matchabilities #-}

module Map1 where

import Data.Kind
import GHC.Exts

type Map1 :: (a -> b) -> [a] -> [b]
type family Map1 f xs where
  Map1 f       '[] =             '[]
  Map1 f (x ': xs) = f x ': Map1 f xs

