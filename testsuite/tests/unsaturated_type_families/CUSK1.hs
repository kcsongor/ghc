{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module CUSK1 where

import Data.Kind

type family Id a where
  Id a = a

type family NoCusk :: (Type -> Type) where
  NoCusk = Maybe
  NoCusk = Id
