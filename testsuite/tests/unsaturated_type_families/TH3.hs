{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
module TH3 where

import Data.Foldable
import Data.Kind
import GHC.Exts
import Language.Haskell.TH hiding (Type)
import System.IO

type family F1 :: Type ->    Type
type family F2 :: Type -> @U Type
type family F3 :: Type -> @M Type
type family F4 :: Type -> @m Type

g1 :: Int -> Int
g1 = undefined

g2 :: Int -> @U Int
g2 = undefined

g3 :: Int -> @M Int
g3 = undefined

g4 :: Int -> @m Int
g4 = undefined

$(do let p n = do i <- reify n
                  runIO $ hPutStrLn stderr $ "Reified info for " ++ nameBase n
                  runIO $ hPrint stderr i
                  runIO $ hPutStrLn stderr ""
     for_ [''F1, ''F2, ''F3, ''F4, 'g1, 'g2, 'g3, 'g4, 'Just] p
     pure [])
