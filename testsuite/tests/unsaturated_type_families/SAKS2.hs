{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# OPTIONS_GHC -fprint-explicit-matchabilities #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module SAKS2 where

import Data.Kind
import GHC.Exts

type family Id a where
  Id a = a

type Foo :: Type -> @m Type
type family Foo :: Type -> Type where
  Foo = Maybe
  Foo = Id
