{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
module UnsaturatedTypeFamiliesSynonymInPattern where

import Data.Kind
import GHC.Exts

type Id1 (a :: Type) = a
type Id2 (a :: Type) = a

type family Distinguish (f :: Type -> @'Unmatchable Type) (a :: Type) (b :: Type) :: Type where
  Distinguish Id1 a _ = a
  Distinguish Id2 _ b = b
