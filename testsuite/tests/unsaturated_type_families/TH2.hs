{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# OPTIONS_GHC -dcore-lint #-}
{-# OPTIONS_GHC -ddump-splices #-}
module TH2 where

import Language.Haskell.TH
import GHC.Exts

_ = $([t| forall k. k -> k |] >>= stringE . pprint)
_ = $([t| forall k. k -> @M k |] >>= stringE . pprint)
_ = $([t| forall k. k -> @U k |] >>= stringE . pprint)
_ = $([t| forall m k. k -> @m k |] >>= stringE . pprint)

_ = $([t| forall k -> k |] >>= stringE . pprint)
_ = $([t| forall k -> @M k |] >>= stringE . pprint)
_ = $([t| forall k -> @U k |] >>= stringE . pprint)
_ = $([t| forall m. forall k -> @m k |] >>= stringE . pprint)
