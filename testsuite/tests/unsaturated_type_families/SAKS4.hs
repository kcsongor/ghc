{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# OPTIONS_GHC -fprint-explicit-matchabilities #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module SAKS4 where

import Data.Kind
import GHC.Exts

-- inferred kind matchable
type Foo :: Type -> Type
type family Foo :: Type -> Type where
  Foo = Maybe
