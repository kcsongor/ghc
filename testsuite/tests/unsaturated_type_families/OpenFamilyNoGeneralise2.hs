{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE TypeFamilies #-}
module OpenFamilyNoGeneralise2 where

import Data.Kind
import GHC.Exts

type Sing1 :: k -> Type
type family Sing1

type Sing2 :: k -> Type
type family Sing2 :: k -> Type

type family Sing3 :: k -> Type

newtype WrappedSing1 a = WrapSing1 (Sing1 a)
newtype WrappedSing2 a = WrapSing2 (Sing2 a)
newtype WrappedSing3 a = WrapSing3 (Sing3 a)
