{-# LANGUAGE AllowAmbiguousTypes     #-}
{-# LANGUAGE DataKinds               #-}
{-# LANGUAGE PolyKinds               #-}
{-# LANGUAGE RankNTypes              #-}
{-# LANGUAGE TypeApplications        #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module UnsaturatedDComp where

import Data.Kind
import GHC.Exts

type DComp a (b :: a -> Type) (c :: forall (x :: a). b x -> Type)
           (f :: forall (x :: a). forall (y :: b x) -> c @x y)
           (g :: forall (x :: a) -> b x)
           (x :: a)
  = f @x (g x)


type D (x :: forall a. Maybe a) = x
