{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module OpenFamilyNoGeneralise where

import Data.Kind

type family Id a where
  Id a = a

type family What :: Type -> Type

type instance What = Maybe
-- this should fail
type instance What = Id
