{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# OPTIONS_GHC -fprint-explicit-matchabilities #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module SAKS10 where

import Data.Kind
import GHC.Exts
import GHC.TypeLits

type Id a = a

-- shouldn't panic
type Poly :: Nat -> (Type -> @m Type) -> Type
type family Poly n (f :: Type -> Type) where
  Poly 0 m = m Int
  Poly 1 f = f (Poly 0 Maybe)
  Poly n f = f (Poly (n - 1) Id)
