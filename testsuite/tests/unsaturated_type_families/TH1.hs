{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -ddump-splices #-}
module TH1 where

import Language.Haskell.TH
import GHC.Exts
import GHC.Types

f :: $(arrowT `appT` conT ''Int `appT` conT ''Int)
f x = x

g :: $(arrowT `appKindT` conT ''U `appT` conT ''Int `appT` conT ''Int)
g x = x
