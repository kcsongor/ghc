{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE PolyKinds #-}
module CUSK4 where

import Data.Kind

type family Cusk :: Type -> k where
  Cusk = Maybe
