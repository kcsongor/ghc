{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
module TypeFamilyFlattening where

import Data.Kind
import Data.Proxy
import Data.Type.Equality
import GHC.Exts

type family F (x :: Type) :: Type where
  F Int = Bool
  F _ = Int

noReduce :: forall (f :: Type -> @U Type) x. F (f x)
noReduce = 10
