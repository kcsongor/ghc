{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}

module Parse where

type family (.) (f :: b -> @m c) (g :: a -> @n b) (x :: a) :: c where
  (f . g) x = f (g x)
infixr 9 .
