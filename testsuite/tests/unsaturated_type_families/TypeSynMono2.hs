{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE PolyKinds #-}
module TypeSynMono2 where

import Data.Kind

type Id a = a

type F = Maybe :: Type -> Type
type G = Id :: Type -> Type
