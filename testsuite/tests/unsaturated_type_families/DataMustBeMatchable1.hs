{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -fprint-explicit-matchabilities #-}

module DataMustBeMatchable1 where

import GHC.Exts
import Data.Kind
import Data.Type.Equality

type family Kool a where
  Kool a = M

data T a :: Type -> @(Kool a) Type where
