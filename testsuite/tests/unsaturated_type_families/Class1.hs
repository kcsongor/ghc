{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -fprint-explicit-matchabilities #-}

module Class1 where

import GHC.Exts
import Data.Kind
import Data.Type.Equality

class Functor f where
  fmap :: (a -> @U b) -> @U f a -> @U f b
