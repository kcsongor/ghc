{-# LANGUAGE GADTs #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}
module VisibleForalls where

import Data.Kind
import Data.Type.Equality

inject :: forall (f :: forall (k :: Type) -> Type)
                 (a :: Type) (b :: Type).
          f a :~: f b
       -> a :~: b
inject Refl = Refl
