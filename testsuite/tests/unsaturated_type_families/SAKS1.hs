{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
module SAKS1 where

import Data.Kind
import GHC.Exts

type X :: k -> k
type family X :: forall k. k -> k
