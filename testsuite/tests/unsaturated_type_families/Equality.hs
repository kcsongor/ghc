{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
module UnsaturatedTypeFamiliesEquality where

import Data.Kind
import Data.Type.Equality
import GHC.Types

eq :: G Int :~: G Int
eq = Refl

type family G :: Type -> @'Unmatchable Type
