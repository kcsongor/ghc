{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE StandaloneKindSignatures #-}
module VisibleForalls2 where

import Data.Kind
import GHC.Types

type family F1 (k :: Type) :: k where {}

-- TODO: this is now failing. Not entirely sure why,
-- but I think because I messed with generaliseTcTyCon?
type F2 :: forall m. forall (k :: Type) -> @m k
type family F2 where
  F2 @Unmatchable = F1
