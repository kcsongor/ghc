{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UnsaturatedTypeFamilies #-}
module Bug23a where

import Data.Kind

type Id a = a
b :: (f x ~ Id) => f x Int
b = 10
