module GHC.Core.Utils where

import GHC.Core.Multiplicity
import GHC.Core.Type

mkFunctionType :: Matchability -> Mult -> Type -> Type -> Type
